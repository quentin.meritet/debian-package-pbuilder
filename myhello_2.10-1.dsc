Format: 1.0
Source: myhello
Binary: myhello
Architecture: any
Version: 2.10-1
Maintainer: Quentin Meritet <meritet.quentin@gmail.com>
Standards-Version: 2.10
Build-Depends: debhelper (>= 9)
Package-List:
 myhello deb devel optional arch=any
Checksums-Sha1:
 9c773054d1056e54b26a85d17caa51f01031b35b 1195 myhello_2.10-1.tar.gz
Checksums-Sha256:
 0f6de6ded48429d1132b7a21f9c02d6bc4e40efab745c00452bc2b27c8ecf947 1195 myhello_2.10-1.tar.gz
Files:
 ca572624b51ae7fa6d2256f68dada29f 1195 myhello_2.10-1.tar.gz
